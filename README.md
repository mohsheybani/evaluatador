![Pipeline status](https://gitlab.com/erudinsky/evaluatador/badges/main/pipeline.svg)
![Release status](https://gitlab.com/erudinsky/evaluatador/-/badges/release.svg)

# Evaluatador

Evaluatador is an evaluator for Azure DevOps. It uses [Azure DevOps Services REST API](https://learn.microsoft.com/en-us/rest/api/azure/devops/?view=azure-devops-rest-7.2) for querying different parts of Azure DevOps and provide summary. The tool's primary goal is to help [GitLab PS to scope engagement](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/engagement-mgmt/scoping-information/migrations/#team-foundation-server-tfsazure-devops-ado-to-gitlab), however the tool can be used for any other tasks when assessment of ADO organization and projects is required. 

## How to use

The tool expects the following environment variables: 

* `AZURE_DEVOPS_ORG_URL` - The URL of the organization
* `AZURE_DEVOPS_EXT_PAT` - The personal access token for the organization with **read** scope

Find out more infromation how to generate personal access token in [Microsoft's official documentation](https://learn.microsoft.com/en-us/azure/devops/organizations/accounts/use-personal-access-tokens-to-authenticate?view=azure-devops&tabs=Windows). 

[Download binary](https://gitlab.com/erudinsky/evaluatador/-/releases) for the required architecture and run:

```bash

chmod +x evaluatador

# Fecth information about all repos:

./evaluatador repos

Repos: (_apis/git/repositories):
+---+--------------------------------------+------------------+-------------------------+--------+-------------------------------+
| # | ID                                   | PROJECT          | NAME                    | SIZE   | DEFAULT BRANCH                |
+---+--------------------------------------+------------------+-------------------------+--------+-------------------------------+
| 1 | eb96368e-87df-4744-9448-023a1ae6eb53 | MyShuttle        | MyShuttle               | 1.7 MB | refs/heads/master             |
| 2 | 08463344-9ae8-4cc4-92d8-72a94dcaecef | SmartHotel360    | PublicWeb               | 7.6 MB | refs/heads/master             |
| 3 | 093bd023-9e62-41ef-9a2c-198b1f33db4a | PartsUnlimited   | IAC                     | 725 B  | refs/heads/main               |
| 4 | df0d5b4c-3d49-4990-89a6-441afd400333 | PartsUnlimited   | PartsUnlimited          | 9.1 MB | refs/heads/master             |
| 5 | 2daefb22-3dd2-414a-865c-44efe9b15b7e | Tailwind Traders | TailwindTraders-Website | 16 MB  | refs/heads/create-cicd-action |
| 6 | 442ff888-9085-4cab-a997-71701ce779dd | Tailwind Traders | TailwindTraders-Backend | 30 MB  | refs/heads/custom-metrics-hpa |
+---+--------------------------------------+------------------+-------------------------+--------+-------------------------------+

# Fecth information about all projects:

Projects (/_apis/projects):
+---+--------------------------------------+------------------+------+----------+-----------------------------------+
| # | ID                                   | NAME             | SCM  | TEMPLATE | LAST UPDATE                       |
+---+--------------------------------------+------------------+------+----------+-----------------------------------+
| 0 | 69c09eba-78ed-4d1f-a14f-1d6a61457caa | MyShuttle        | Git  | Scrum    | 2023-11-21 21:40:02.243 +0000 UTC |
| 1 | 74e0f300-2cd6-48ba-9bb3-ca59ba215a59 | SmartHotel360    | Git  | Scrum    | 2023-11-21 21:27:41.873 +0000 UTC |
| 2 | 5ed5db87-da30-4ba3-bc54-d8502b8be5a4 | PartsUnlimited   | Git  | Scrum    | 2023-11-21 21:30:53.297 +0000 UTC |
| 3 | 137fa6a4-04ba-4d3c-bad0-3fc6d39e958d | Tailwind Traders | Git  | Agile    | 2023-11-21 21:38:15.623 +0000 UTC |
| 4 | f571fd44-19a9-47ff-9bad-dafc0fa507fb | Dadjokes         | Tfvc | CMMI     | 2023-11-29 09:19:51.067 +0000 UTC |
+---+--------------------------------------+------------------+------+----------+-----------------------------------+

# Fecth help information about "artifacts" command:

./evaluatador artifacts --help

# To list all commands: 

./evaluatador --help

# Generate Excel report:

./evaluatador report

```

## Contacts

Tag @erudinsky for anything related to this tool.