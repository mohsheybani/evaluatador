package project

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/microsoft/azure-devops-go-api/azuredevops/v7"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/core"
)

func GetAllProjects() []string {
	projectIDs := []string{}

	organizationURL := os.Getenv("AZURE_DEVOPS_ORG_URL")
	personalAccessToken := os.Getenv("AZURE_DEVOPS_EXT_PAT")

	connection := azuredevops.NewPatConnection(organizationURL, personalAccessToken)
	ctx := context.Background()
	client, err := core.NewClient(ctx, connection)
	if err != nil {
		log.Fatal(err)
	}

	responseValue, err := client.GetProjects(ctx, core.GetProjectsArgs{})
	if err != nil {
		log.Fatal(err)
	}
	index := 0
	for responseValue != nil {
		for _, teamProjectReference := range (*responseValue).Value {
			index++

			projectIDs = append(projectIDs, fmt.Sprint(*teamProjectReference.Id))

		}
		if responseValue.ContinuationToken != "" {
			continuationToken, err := strconv.Atoi(responseValue.ContinuationToken)
			if err != nil {
				log.Fatal(err)
			}
			projectArgs := core.GetProjectsArgs{
				ContinuationToken: &continuationToken,
			}
			responseValue, err = client.GetProjects(ctx, projectArgs)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			responseValue = nil
		}
	}
	return projectIDs
}
