package client

import (
	"context"
	"log"

	"github.com/microsoft/azure-devops-go-api/azuredevops/v7"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/core"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/feed"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/git"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/graph"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/memberentitlementmanagement"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/pipelines"
)

func CreatePipelinesClient(organizationURL string, personalAccessToken string) (pipelines.Client, context.Context) {
	connection := azuredevops.NewPatConnection(organizationURL, personalAccessToken)
	ctx := context.Background()
	client := pipelines.NewClient(ctx, connection)
	return client, ctx
}

func CreateCoreClient(organizationURL string, personalAccessToken string) (core.Client, context.Context) {
	connection := azuredevops.NewPatConnection(organizationURL, personalAccessToken)
	ctx := context.Background()
	client, err := core.NewClient(ctx, connection)
	if err != nil {
		log.Fatal(err)
	}
	return client, ctx
}

func CreateGitClient(organizationURL string, personalAccessToken string) (git.Client, context.Context) {
	connection := azuredevops.NewPatConnection(organizationURL, personalAccessToken)
	ctx := context.Background()
	client, err := git.NewClient(ctx, connection)
	if err != nil {
		log.Fatal(err)
	}
	return client, ctx
}

func CreateFeedClient(organizationURL string, personalAccessToken string) (feed.Client, context.Context) {
	connection := azuredevops.NewPatConnection(organizationURL, personalAccessToken)
	ctx := context.Background()
	client, err := feed.NewClient(ctx, connection)
	if err != nil {
		log.Fatal(err)
	}
	return client, ctx
}

func CreateGraphClient(organizationURL string, personalAccessToken string) (graph.Client, context.Context) {
	connection := azuredevops.NewPatConnection(organizationURL, personalAccessToken)
	ctx := context.Background()
	client, err := graph.NewClient(ctx, connection)
	if err != nil {
		log.Fatal(err)
	}
	return client, ctx
}

func CreateUserMemberentitlementmanagementClient(organizationURL string, personalAccessToken string) (memberentitlementmanagement.Client, context.Context) {
	connection := azuredevops.NewPatConnection(organizationURL, personalAccessToken)
	ctx := context.Background()
	client, err := memberentitlementmanagement.NewClient(ctx, connection)
	if err != nil {
		log.Fatal(err)
	}
	return client, ctx
}
