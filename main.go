/*
Copyright © 2023 NAME HERE <erudinsky+evaluatador@gitlab.com>
*/
package main

import "gitlab.com/erudinsky1/evaluatador/cmd"

func main() {
	cmd.Execute()
}
