package cmd

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/core"
	"github.com/spf13/cobra"
	"github.com/xuri/excelize/v2"
	"gitlab.com/erudinsky1/evaluatador/internal/client"
	"gitlab.com/erudinsky1/evaluatador/internal/project"
)

var reportCmd = &cobra.Command{
	Use:   "report",
	Short: "A command to excel generate report.",
	Long:  `A command to excel generate report.`,
	Run: func(cmd *cobra.Command, args []string) {

		projectIDs := project.GetAllProjects()
		client, ctx := client.CreateCoreClient(os.Getenv("AZURE_DEVOPS_ORG_URL"), os.Getenv("AZURE_DEVOPS_EXT_PAT"))

		f := excelize.NewFile()
		defer func() {
			if err := f.Close(); err != nil {
				fmt.Println(err)
			}
		}()

		// Set style for headers
		header, err := f.NewStyle(&excelize.Style{
			Fill: excelize.Fill{
				Type:    "pattern",
				Color:   []string{"#000000"},
				Pattern: 1,
			},
			Font: &excelize.Font{
				Bold:  true,
				Color: "FFFFFF",
			},
		})
		if err != nil {
			fmt.Println(err)
		}

		// Set value of a cell.

		f.SetCellValue("Sheet1", "A1", "Short summary")
		f.SetColWidth("Sheet1", "A", "A", 15)
		f.SetColWidth("Sheet1", "B", "B", 30)

		f.SetCellValue("Sheet1", "A2", "Date of run")
		f.SetCellValue("Sheet1", "B2", time.Now().Format("02-01-2006 15:04:05 MST"))

		f.SetCellValue("Sheet1", "A4", "Number of projects")
		f.SetCellValue("Sheet1", "B4", len(projectIDs))

		err = f.SetCellStyle("Sheet1", "A1", "B1", header)
		if err != nil {
			fmt.Println(err)
		}

		f.SetSheetName("Sheet1", "General Information")

		// Create Project's report

		if len(projectIDs) != 0 {
			// Create a new sheet.
			index, err := f.NewSheet("Projects")
			if err != nil {
				fmt.Println(err)
				return
			}
			err = f.SetCellStyle("Projects", "A1", "F1", header)
			if err != nil {
				fmt.Println(err)
			}

			f.SetCellValue("Projects", "A1", "#")
			f.SetCellValue("Projects", "B1", "ID")
			f.SetCellValue("Projects", "C1", "NAME")
			f.SetCellValue("Projects", "D1", "SCM")
			f.SetCellValue("Projects", "E1", "TEMPLATE")
			f.SetCellValue("Projects", "F1", "LAST UPDATE")
			f.SetColWidth("Projects", "A", "A", 5)
			f.SetColWidth("Projects", "B", "B", 40)
			f.SetColWidth("Projects", "C", "C", 40)
			f.SetColWidth("Projects", "D", "D", 5)
			f.SetColWidth("Projects", "E", "E", 10)
			f.SetColWidth("Projects", "F", "F", 40)
			// Google Drive's width was 1/7 (i.e. 5 here is 35 in google drive)

			f.SetActiveSheet(index)

			i := 2
			for _, projectID := range projectIDs {

				true := true

				responseValue, err := client.GetProject(ctx, core.GetProjectArgs{ProjectId: &projectID, IncludeCapabilities: &true})
				if err != nil {
					log.Fatal(err)
				}

				capabilities := map[string]string{}

				for _, value := range *responseValue.Capabilities {
					for key, value := range value {
						capabilities[key] = value
					}
				}
				cellA := fmt.Sprintf("A%v", i)
				cellB := fmt.Sprintf("B%v", i)
				cellC := fmt.Sprintf("C%v", i)
				cellD := fmt.Sprintf("D%v", i)
				cellE := fmt.Sprintf("E%v", i)
				cellF := fmt.Sprintf("F%v", i)
				f.SetCellValue("Projects", cellA, i-1)
				f.SetCellValue("Projects", cellB, *responseValue.Id)
				f.SetCellValue("Projects", cellC, *responseValue.Name)
				f.SetCellValue("Projects", cellD, capabilities["sourceControlType"])
				f.SetCellValue("Projects", cellE, capabilities["templateName"])
				f.SetCellValue("Projects", cellF, *responseValue.LastUpdateTime)
				i++
			}

		}

		filename := fmt.Sprintf("AzureDevOpsReport_%s.xlsx", time.Now().Format("02-01-2006_15-04-05_MST"))

		if err := f.SaveAs(filename); err != nil {
			fmt.Println(err)
		}
		fmt.Printf("📁 Report was saved as %v\n", filename)

	},
}

func init() {
	rootCmd.AddCommand(reportCmd)
}
