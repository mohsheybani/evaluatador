package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/memberentitlementmanagement"
	"github.com/spf13/cobra"
	"gitlab.com/erudinsky1/evaluatador/internal/client"
)

// usersCmd represents the users command
var usersCmd = &cobra.Command{
	Use:   "users",
	Short: "A command to collect information about Users.",
	Long:  `A command to collect information about Users.`,
	Run: func(cmd *cobra.Command, args []string) {

		memberentitlementmanagementClient, ctx := client.CreateUserMemberentitlementmanagementClient(os.Getenv("AZURE_DEVOPS_ORG_URL"), os.Getenv("AZURE_DEVOPS_EXT_PAT"))

		responseValue, err := memberentitlementmanagementClient.SearchUserEntitlements(ctx, memberentitlementmanagement.SearchUserEntitlementsArgs{})
		if err != nil {
			log.Fatal(err)
		}

		t := table.NewWriter()
		t.AppendHeader(table.Row{"#", "Email", "License", "Status", "Origin", "Created", "Last acitity"})

		for index, user := range *responseValue.Members {

			index++

			t.AppendRow(table.Row{index, *user.User.MailAddress, *user.AccessLevel.LicenseDisplayName, *user.AccessLevel.Status, *user.User.Origin, *user.DateCreated, *user.LastAccessedDate})

		}

		fmt.Println("Users: (_apis/userentitlements):")
		fmt.Println(t.Render())

	},
}

func init() {
	rootCmd.AddCommand(usersCmd)
}
