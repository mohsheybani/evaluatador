package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/core"
	"github.com/spf13/cobra"
	"gitlab.com/erudinsky1/evaluatador/internal/client"
	"gitlab.com/erudinsky1/evaluatador/internal/project"
)

// projectsCmd represents the projects command
var projectsCmd = &cobra.Command{
	Use:   "projects",
	Short: "A command to collect information about Projects.",
	Long:  `A command to collect information about Projects.`,
	Run: func(cmd *cobra.Command, args []string) {

		projectIDs := project.GetAllProjects()
		client, ctx := client.CreateCoreClient(os.Getenv("AZURE_DEVOPS_ORG_URL"), os.Getenv("AZURE_DEVOPS_EXT_PAT"))

		if len(projectIDs) != 0 {
			fmt.Println("Projects (/_apis/projects):")
			t := table.NewWriter()
			t.AppendHeader(table.Row{"#", "ID", "Name", "SCM", "Template", "Last update"})
			index := 0
			for _, projectID := range projectIDs {

				true := true

				responseValue, err := client.GetProject(ctx, core.GetProjectArgs{ProjectId: &projectID, IncludeCapabilities: &true})
				if err != nil {
					log.Fatal(err)
				}

				capabilities := map[string]string{}

				for _, value := range *responseValue.Capabilities {
					for key, value := range value {
						capabilities[key] = value
					}
				}

				t.AppendRow(table.Row{index, *responseValue.Id, *responseValue.Name, capabilities["sourceControlType"], capabilities["templateName"], *responseValue.LastUpdateTime})

				index++

			}
			fmt.Println(t.Render())

		}
	},
}

func init() {
	rootCmd.AddCommand(projectsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// projectsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// projectsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
