package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/pipelines"
	"github.com/spf13/cobra"
	"gitlab.com/erudinsky1/evaluatador/internal/client"
	"gitlab.com/erudinsky1/evaluatador/internal/project"
)

var pipelinesCmd = &cobra.Command{
	Use:   "pipelines",
	Short: "A command to collect information about Pipelines.",
	Long:  `A command to collect information about Pipelines.`,
	Run: func(cmd *cobra.Command, args []string) {

		projectIDs := project.GetAllProjects()
		client, ctx := client.CreatePipelinesClient(os.Getenv("AZURE_DEVOPS_ORG_URL"), os.Getenv("AZURE_DEVOPS_EXT_PAT"))
		index := 0

		if len(projectIDs) != 0 {

			fmt.Println("Pipelines (/_apis/pipelines):")

			t := table.NewWriter()
			t.AppendHeader(table.Row{"#", "Project ID / Location", "Name", "Type", "ID"})

			for _, projectID := range projectIDs {

				responseValue, err := client.ListPipelines(ctx, pipelines.ListPipelinesArgs{Project: &projectID})
				if err != nil {
					log.Fatal(err)
				}
				for _, field := range *responseValue {
					index++
					pipeline, err := client.GetPipeline(ctx, pipelines.GetPipelineArgs{Project: &projectID, PipelineId: field.Id})
					if err != nil {
						log.Fatal(err)
					}
					t.AppendRow(table.Row{index, fmt.Sprintf("%s%s", projectID, *pipeline.Folder), *pipeline.Name, *pipeline.Configuration.Type, *pipeline.Id})
				}

			}
			fmt.Println(t.Render())

		} else {
			fmt.Println("No pipelines found")
		}

	},
}

func init() {
	rootCmd.AddCommand(pipelinesCmd)
}
