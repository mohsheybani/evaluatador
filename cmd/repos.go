package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/dustin/go-humanize"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/git"
	"github.com/spf13/cobra"
	"gitlab.com/erudinsky1/evaluatador/internal/client"
	"gitlab.com/erudinsky1/evaluatador/internal/project"
)

// reposCmd represents the repos command
var reposCmd = &cobra.Command{
	Use:   "repos",
	Short: "A command to collect information about Repos.",
	Long:  `A command to collect information about Repos.`,
	Run: func(cmd *cobra.Command, args []string) {

		projectIDs := project.GetAllProjects()
		client, ctx := client.CreateGitClient(os.Getenv("AZURE_DEVOPS_ORG_URL"), os.Getenv("AZURE_DEVOPS_EXT_PAT"))

		t := table.NewWriter()
		t.AppendHeader(table.Row{"#", "ID", "Project", "Name", "Size", "Default branch"})

		index := 0

		for i := 0; i < len(projectIDs); i++ {
			repoList, err := client.GetRepositories(ctx, git.GetRepositoriesArgs{Project: &projectIDs[i]})
			if err != nil {
				log.Fatal(err)
			}

			for _, repo := range *repoList {
				index++
				if err != nil {
					log.Fatal(err)
				}

				t.AppendRow(table.Row{index, *repo.Id, *repo.Project.Name, *repo.Name, humanize.Bytes(*repo.Size), *repo.DefaultBranch})
			}

		}

		fmt.Println("Repos: (_apis/git/repositories):")
		fmt.Println(t.Render())

	},
}

func init() {
	rootCmd.AddCommand(reposCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// reposCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// reposCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
