package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/microsoft/azure-devops-go-api/azuredevops/v7/feed"
	"github.com/spf13/cobra"
	"gitlab.com/erudinsky1/evaluatador/internal/client"
	"gitlab.com/erudinsky1/evaluatador/internal/project"
)

// artifactsCmd represents the artifacts command
var artifactsCmd = &cobra.Command{
	Use:   "artifacts",
	Short: "A command to collect information about Artifacts.",
	Long:  `A command to collect information about Artifacts.`,
	Run: func(cmd *cobra.Command, args []string) {

		projectIDs := project.GetAllProjects()
		client, ctx := client.CreateFeedClient(os.Getenv("AZURE_DEVOPS_ORG_URL"), os.Getenv("AZURE_DEVOPS_EXT_PAT"))

		t := table.NewWriter()
		t.AppendHeader(table.Row{"#", "Project", "ID", "Name"})

		index := 0

		for _, projectID := range projectIDs {

			feedList, err := client.GetFeeds(ctx, feed.GetFeedsArgs{Project: &projectID})
			if err != nil {
				log.Fatal(err)
			}

			for _, feed := range *feedList {
				index++
				if err != nil {
					log.Fatal(err)
				}

				t.AppendRow(table.Row{index, *feed.Project.Name, *feed.Id, *feed.Name})
			}

		}

		fmt.Println("Feeds: (_apis/packaging/feeds):")
		fmt.Println(t.Render())
	},
}

func init() {
	rootCmd.AddCommand(artifactsCmd)
}
